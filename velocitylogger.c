#include "slippybike.h"

#include <time.h>
#include <sys/time.h>

void *velocity_logger(__attribute__ ((unused)) void* param) {
  FILE *file_gps = fopen("/var/www/html/velocity_gps.csv", "a");
  FILE *file_reed= fopen("/var/www/html/velocity_reed.csv", "a");
  char timestr[26];
  int microsecond;
  struct tm* tm_info;
  struct timeval tv;
  while(true) {

    gettimeofday(&tv, NULL);

    tm_info = localtime(&tv.tv_sec);

    microsecond = tv.tv_usec;
    strftime(timestr, 26, "%Y-%m-%dT%H:%M:%S", tm_info);
    fprintf(file_gps, "%s.%06d,%f\n", timestr, microsecond, g_velocity);
    fprintf(file_reed, "%s.%06d,%f\n", timestr, microsecond, r_velocity);
    fflush(file_gps);
    fflush(file_reed);

    usleep(100000);
  }
}
