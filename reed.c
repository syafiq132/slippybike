#include "slippybike.h"
#include <bcm2835.h>
#include <stdio.h>
#include <time.h>

#define PIN 18

void *reed(__attribute__ ((unused)) void* param) {
  //set the pin to input
  bcm2835_gpio_fsel(PIN, BCM2835_GPIO_FSEL_INPT);
  //pullup?
  bcm2835_gpio_set_pud(PIN, BCM2835_GPIO_PUD_UP);

  int bswitch = 0;
  int prev_bswitch = 0;
  float current_speed;
  //float temp;
  long long int delta_time = *g_timer, prev_time = 0, next_time = 1E16;
  int t_flag;

  while (1) {
    //printf("test");
    //printf("%d ", counter);
    bswitch = bcm2835_gpio_lev(PIN);

    if(next_time < *g_timer && t_flag == 0){
      printf("timeout \n");
      //sleep(1);
      r_velocity = 0;
      slip_detector();
      t_flag = 1;
    }

    if(bswitch != prev_bswitch) {
      if(bswitch == 1){
        delta_time = *g_timer - prev_time;
        //conversion to speed
        //printf("delta_time: %lld\n", delta_time);
        //printf("next_time: %lld\n", next_time);
        //printf("clock: %lld\n", *g_timer);
        //temp = delta_time*CLOCKS_PER_SEC;
        //printf("temp : %f \n", temp);
        //printf("clocks per sec : %f \n", CLOCKS_PER_SEC);
        current_speed = 2.07*1E6/delta_time;
        r_velocity = current_speed*3.6;
        printf("current speed : %f\n", current_speed*3.6);
        //update dynamic timeout
        next_time = *g_timer + 2*delta_time;
        //update prev for next loop
        prev_time = *g_timer;
        sleep(1);
      }
      t_flag = 0;
      prev_bswitch = bswitch;
    }
  }
}
