#include "slippybike.h"
#include "minmea.h"

float knots_to_meters_per_second(float knots) {
  return knots * 1852 / 3600;
}

float kph_to_meters_per_second(float kph) {
  return kph / 3.6;
}

void *gps_parser(__attribute__ ((unused)) void* param) {
  char line[MINMEA_MAX_LENGTH];
  while (fgets(line, sizeof(line), stdin) != NULL) {
    DEBUG("%s", line);
    switch (minmea_sentence_id(line, false)) {
    case MINMEA_SENTENCE_RMC: {
      struct minmea_sentence_rmc frame;
      if (minmea_parse_rmc(&frame, line)) {

        float latitude = minmea_tocoord(&frame.latitude);
        float longitude = minmea_tocoord(&frame.longitude);
        float velocity = knots_to_meters_per_second(minmea_tofloat(&frame.speed));
        DEBUG("$xxRMC degree coordinates and speed: (%f,%f) %f\n",
               latitude, longitude, velocity);
        g_latitude = latitude;
        g_longitude = longitude;
        g_velocity = velocity;

      } else {
        DEBUG("$xxRMC sentence is not parsed\n");
      }
      break;
    }
    case MINMEA_SENTENCE_VTG: {
      struct minmea_sentence_vtg frame;
      if (minmea_parse_vtg(&frame, line)) {
        float velocity = kph_to_meters_per_second(minmea_tofloat(&frame.speed_kph));
        DEBUG("$xxVTG speed: %f\n", velocity);
        g_velocity = velocity;

      } else {
        DEBUG("$xxVTG sentence is not parsed\n");
      }
      break;
    }
    default:
      DEBUG("$xxxxx sentence is not parsed\n");
      break;
    }
	printf("gps velocity %f \n", g_velocity);
  }
}
