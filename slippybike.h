#ifndef SLIP_H_
#define SLIP_H_

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

// Debug macro
#ifdef DEBUG_BUILD
#  define DEBUG(args ...) fprintf(stderr, args)
#else
#  define DEBUG(args ...)
#endif

// Global velocity variable in meters per second
volatile float g_velocity; // speed from gps
volatile float r_velocity; // speed from reed

// Global coordinate variables
volatile float g_latitude;
volatile float g_longitude;

// Global timer pointer, see map_timer.c
long long int* g_timer;

void somefunction();
void *reed(void*);
void *slip_detector(void);
void *gps_parser(void*);
void *accelerometer_reader(void*);
void *velocity_logger(void* param);
long long int *map_timer(void);

#endif
